<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Sistema Colegio</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Avant" />
	<meta name="author" content="The Red Team" />

    <link href="./Avant/assets/css/styles.min.css" rel="stylesheet" type='text/css' media="all" />
    <link href='./img/logo_colegio.png' rel='shortcut icon' type='image/x-icon'>

    <link href='./Avant/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher' /> 
    <link href='./Avant/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher' /> 
    
	

<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/form-select2/select2.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/form-multiselect/css/multi-select.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/form-daterangepicker/daterangepicker-bs3.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/js/jqueryui.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/codeprettifier/prettify.css' /> 
<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/form-toggle/toggles.css' /> 
<link rel='stylesheet' type='text/css' href='./css/ingresoRoot.css' />

	<style>
	    	</style>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" /></head>

<body class="">
    <header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="navbar-header pull-left col-md-7">
            <div class="col-md-offset-1 col-md-2">
            
                
                <img id="imagenLogo" src="./img/logo_colegio.png" alt="logo colegio augusto pi suñer" width="90" height="95" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="bottom" data-content="El liceo bolivariano augusto pi Suñer está Centrada en un servicio educativo y eficaz el cual contribuya con la formación de los alumnos el cual construya una disciplina con el objetivo de alcanzar un nivel educativo virtuoso, que sustente valores, ética y un modelo de educación con calidad.">
            
            </div>
            <div class="col-md-12">
                <h1><span>Liceo Augusto Pi Suñer</span></h1>
            </div>
        </div>
    </header>


    <div id="page-container">
        <!-- BEGIN SIDEBAR -->
        <nav id="page-leftbar" role="navigation">
                <!-- BEGIN SIDEBAR MENU -->
            <ul class="acc-menu" id="sidebar">
                <li><a href="ingreso.php"><i class="icon-home"></i> <span>P&aacute;gina de Inicio</span></a></li>
                <li><a href="javascript:;"><i class="icon-group"></i> <span>Men&uacute; Alumnos</span> </a>
                    <ul class="acc-menu">
                        <li>
                            <a href="newAlmProf.php"><i class="icon-plus"></i><span>Agregar Alumno</span></a>
                        </li>
                        <li>
                            <a href="modifAlmProf.php"><i class="icon-pencil"></i><span>Modificar Alumno</span></a>
                        </li>
                        <li>
                            <a href="califAlmProf.php"><i class="icon-file"></i><span>Agregar Notas</span></a>
                        </li>
                        <li>
                            <a href="bolAlmProf.php"><i class="icon-book"></i><span>Bolet&iacute;n</span></a>
                        </li>
                    </ul>
                </li>
                <li><a href="respaldo.php"><i class="icon-copy"></i> <span>Respaldo</span> </a>
                </li>

            </ul>
            <!-- END SIDEBAR MENU -->
        </nav>

<div id="page-content">
    <div id='wrap'>
			<div id="page-heading">
            <h1>Deshabilitar</h1>
			</div>

        <div class="container">

			<div class="panel panel-midnightblue">
				<div class="panel-heading">
					<h4>Deshabilitar Alumno</h4>
				</div>
				<div class="panel-body collapse in">
					<form action="" method ="POST" class="form-horizontal row-border">
								
						<div class="form-group">
							<label for="fieldemail" class="col-md-3 control-label">Indique la c&eacute;dula del alumno a deshabilitar:</label>
							<div class="col-md-6"><input name="borrar" id="borrar" class="form-control" type="integer" placeholder="C&eacute;dula" name="borrar" required="" /></div>
						</div>
						
						<div class="panel-footer">
					
							
							<input type="submit" name="submit" class="finish btn-success btn" value="Deshabilitar" />
					</form>
					
						</div>
				<?php
					if (isset($_POST['submit'])){
						//require("alerta.php");
						$borrar = $_POST['borrar'];
						require("conexion_db.php");
                        // 1 - habilitado ; 0 - deshabilitado
						$query = "UPDATE alumnos SET Estatus = '0' WHERE CI = '$borrar'";  
						$result = mysql_query($query);  
						mysql_close($con);
						echo '<br>El registro ha sido deshabilitado con exito.';
					}
				?>
			</div>
	
        </div> <!-- container -->

    </div> <!--wrap -->

</div> <!-- page-content -->


    <!--<footer role="contentinfo">
        <div class="clearfix">
            <ul class="list-unstyled list-inline">
                <li>Sistema Colegio</li>
                <button class="pull-right btn btn-inverse btn-xs" id="back-to-top" style="margin-top: -1px; text-transform: uppercase;"><i class="icon-arrow-up"></i></button>
            </ul>
        </div>
    </footer>-->

</div> <!-- page-container -->

<script type='text/javascript' src='./Avant/assets/js/jquery-1.10.2.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jqueryui-1.10.3.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/enquire.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.cookie.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.touchSwipe.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.nicescroll.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/codeprettifier/prettify.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/easypiechart/jquery.easypiechart.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/sparklines/jquery.sparklines.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-toggle/toggle.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-multiselect/js/jquery.multi-select.min.js'></script>
<script type='text/javascript' src='./Avant/assets/plugins/form-validation/jquery.validate.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-stepy/jquery.stepy.js'></script> 
<script type='text/javascript' src='./Avant/assets/demo/demo-formwizard.js'></script>  
<script type='text/javascript' src='./Avant/assets/plugins/quicksearch/jquery.quicksearch.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-typeahead/typeahead.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-select2/select2.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-autosize/jquery.autosize-min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-ckeditor/ckeditor.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-colorpicker/js/bootstrap-colorpicker.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/jqueryui-timepicker/jquery.ui.timepicker.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-daterangepicker/daterangepicker.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-daterangepicker/moment.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/demo/demo-formcomponents.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/placeholdr.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/application.js'></script> 
<script type='text/javascript' src='./Avant/assets/demo/demo.js'></script> 

</body>
</html>
