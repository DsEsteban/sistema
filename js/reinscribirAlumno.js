$(document).ready(function(){

  $("#alumno").inputmask("99999999");
  $.ajax({
    type: "POST",
    url: "http://localhost/Sistema/requestAjax.php",
    data: {accion: '11'},
    success: function(response){
      var info = response.split("/");
      $.each(info, function(index, value){
        var par = value.split("-");
        $("#materias").append(
          $("<option>").attr('value',par[1]).html(par[0])
        );
      });
      $('#materias').multiSelect();
    },
    error: function(response){
      alert(response);
    }
  });

  $("#reinscribir").click(function(){
    if ($("#alumno").val()) {
      var cedula = $("#alumno").val();
      ciAlum = $("#alumno").val();
      if($("#materias").val()){
        var codigos = "";
        $.each($("#materias").val(), function(index, value){
          codigos = codigos + value + "/";
        });
        $.ajax({
          type: "POST",
          url: "http://localhost/Sistema/requestAjax.php",
          data: {accion: '14', cedula: ''+cedula, materias: ''+codigos},
          success: function(response){
            if(response.indexOf("3") > 0){
              alert("El alumno elegido se encuentra deshabilitado.")
            }else{
              alert("El alumno ha sido reinscrito.");
              window.open("http://localhost/Sistema/reporteInscr.php?id="+ciAlum);
              location.href = "http://localhost/Sistema/reinscribirAlumno.php";
            } 
              
          },
          error: function(response){
            alert("Error. Contacte con su administrador del sistema.");
          }
        }); 
      }else{
        alert("Indique la materias que desea introducir");
      } 
    }else{
      alert("Indique una cedula que desea reinscribir");
    }
    return false;
  });
  
  
  
});