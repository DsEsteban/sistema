var ci = 1;

$(document).ready(function(){
  
  $.ajax({
    type: "POST",
    url: "http://localhost/Sistema/requestAjax.php",
    data: {accion: '11'},
    success: function(response){
      var info = response.split("/");
      $("#slctDeshabilitar").append(
          $("<option>").attr('value','').html('[SELECCIONE]')
        );
      $.each(info, function(index, value){
        var par = value.split("-");
        $("#slctDeshabilitar").append(
          $("<option>").attr('value',par[1]).html(par[0])
        );
    });
    },
    error: function(response){
      alert(response);
    }
  });  

  $("#btnDeshabilitar").click(function(){
    if ($("#slctDeshabilitar").val()) {
      var codigo = $("#slctDeshabilitar").val();
      $.ajax({
        type: "POST",
        url: "http://localhost/Sistema/requestAjax.php",
        data: {accion: '12', codigo: ''+codigo},
        success: function(response){
          alert("La materia ha sido deshabilitada");
        },
        error: function(response){
          alert("Error. Contacte con su administrador del sistema.");
        }
      }); 
    }else{
      alert("Indique la materia que desea habilitar");
    }
    return false;
  });
});