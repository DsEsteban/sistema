$(document).ready(function(){
	$("#cedula").inputmask("99999999");
	$("#nota").inputmask("99");

	$.ajax({
	    type: "POST",
	    url: "http://localhost/Sistema/requestAjax.php",
	    data: {accion: '11'},
	    success: function(response){
	      var info = response.split("/");
	      $("#materia").append(
	          $("<option>").attr('value','').html('[SELECCIONE]')
	        );
	      $.each(info, function(index, value){
	        var par = value.split("-");
	        $("#materia").append(
	          $("<option>").attr('value',par[1]).html(par[0])
	        );
	    });
	    },
	    error: function(response){
	      alert(response);
	    }
  	});

	$("#enviar").click(function(){
		ci = $("#cedula").val();	
		not = $("#nota").val();	
		mate = $("#materia").val();	
		if(ci){
			if (not < 21 && not > 0) {

				$.ajax({
					type: "POST",
				    url: "http://localhost/Sistema/requestAjax.php",
				    data: {accion: '3', cedula: ''+ ci, nota: ''+ not, materia: ''+ mate},
				    success: function(response){
				    	if (response.indexOf("3") > 0) {
				    		alert("Alumno Deshabilitado. No se pudo modificar la nota");
				    		
				    	}else{
				     		alert('Nota modificada');
				    	};
				    },
				    error: function(response){
				    	console.log(response);
				    }
			    });
			}else{
				alert("por favor. revisar la nota del alumno.");
			}

		}else{
			alert("por favor. revisar la cedula del alumno.");
		}	
	});
});

document.getElementById("cedula").addEventListener("keyup", function(e){
	var ci = document.getElementById("cedula").value;				
	console.log(ci);
	if (ci) {
		$.ajax({
			type: "POST",
		    url: "http://localhost/Sistema/requestAjax.php",
		    data: {accion: '25', cedula: ci},
		    success: function(response){
		    	if (response.trim() == ",") {
		    		$("#innp_nombre_div").hide(300);
		    		
		    	}else{
			    	$("#innp_nombre").val(response);
			    	$("#innp_nombre_div").show(300);
		    	};

		    }
		});
	};
});