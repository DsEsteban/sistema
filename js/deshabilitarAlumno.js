$(document).ready(function(){
  $("#ciDes").inputmask("99999999");
  $("#cedulaDes").click(function(){
    if ($("#ciDes").val()) {
      var codigo = $("#ciDes").val();
      $.ajax({
        type: "POST",
        url: "http://localhost/Sistema/requestAjax.php",
        data: {accion: '16', cedula: ''+codigo},
        success: function(response){
          
          if(response == '0'){
            alert("la cedula introducida no corresponde con un alumno en sistema")
          }else{
            alert("El alumno ha sido deshabilitada");
          }

        },
        error: function(response){
          alert("Error. Contacte con su administrador del sistema.");
        }
      }); 
    }else{
      alert("Indique el alumno que desea habilitar");
    }
    return false;
  });
});