$(document).ready(function(){
	$("#imagenLogo").popover({
      trigger: 'hover',
      html: true,
      title: 'Reseña histórica'});

	$.ajax({
	    type: "POST",
	    url: "http://localhost/Sistema/requestAjax.php",
	    data: {accion: '11'},
	    success: function(response){
	      var info = response.split("/");
	      $.each(info, function(index, value){
	        var par = value.split("-");
	        $("#materias").append(
	          $("<option>").attr('value',par[1]).html(par[0])
	        );
	      });
	      $('#materias').multiSelect();
	    }
	});
  
	$("#cedula").inputmask("99999999");	

	$('#dialog-confirm').dialog({
		resizable: false,
      	height:150,
		autoOpen: false,
		modal: true,
		buttons: {
      		"Aceptar" : function(){

      			$('#myModal_fecha').modal('show');

      			$("#btn_save_modal").click(function(){
      				var fecha = $("#fecha_camb_lapso").val();
      				if (fecha) {
      					lap = $('#btnLapso').data("lapso") + 1;
						if(lap > 3){
							lap = 1;
							$('#btnLapso').data("lapso", 1)
						}else{
							$('#btnLapso').data("lapso", lap)
						};

		      			$.ajax({
							type: "POST",
							url: "http://localhost/Sistema/requestAjax.php",
							data: {accion: '1', lapso: lap, fechaFin : fecha},
							success: function(response){
								$('#hdLapso').html('');
								$('#hdLapso').html('Lapso Actual: ' + lap);
								$('#fechaPer').html('');
								$('#fechaPer').html(response);
							},
							error: function(response){
								alert("Error. Consulte a su administrador del sistema.");
							}
						});
						$('#dialog-confirm').dialog("close");
						$('#myModal_fecha').modal('hide');
      				}else{
      					alert("por favor, indique una fecha de culminación del lapso");
      				};
      			});
      		},

      		"Cancelar" : function(){
  				$('#dialog-confirm').dialog("close");
      		}
      	}
	});

	//cambiar lapso
	$('#btnLapso').click(function(){
		$("#pText").removeClass("hidden");		
		$('#dialog-confirm').dialog("open");
		return false;
	});

	$("#liPerfil").click(function(){
		$("#dialog-form").removeClass("hidden");
		$('#dialog-form').dialog("open");
		return false
	});
	$("#liAyuda").click(function(){
		$("#dialog-help").removeClass("hidden");
		$('#dialog-help').dialog("open");
		return false
	});

	$('#dialog-form').dialog({
		autoOpen: false,
		modal: true,
		height: 400,
      	width: 550,
		buttons: {
			"Editar" : function(){
				$('#dialog-form').dialog("close");
				var editar = [];
				editar[0] = $("#nombreEdit").val();
				editar[1] = $("#apellidoEdit").val();
				editar[2] = $("#cedulaEdit").val();
				editar[3] = $("#correoEdit").val();
				editar[4] = $("#passEdit").val();
								
				$.ajax({
					type: "POST",
					url: "http://localhost/Sistema/requestAjax.php",
					data: {accion: '7', data: editar}, //, cedula: 
					success: function(response){
						alert("cambios realizados con éxito.");
						
					},
					error: function(response){
						alert("Consulte a su administrador del sistema.");
					}
				});

			},
			"Cancelar" : function(){
				$("fieldset").addClass("hidden");
				$('#dialog-form').dialog("close");
			}
		}

	});

	$('#dialog-help').dialog({
		autoOpen: false,
		modal: true,
		height: 400,
      	width: 300,
      	buttons: {
			"Cerrar" : function(){
				$('#dialog-help').dialog("close");
			}
		}
	});

	$('#dialog-wait').dialog({
		autoOpen: false,
		modal: true,
		height: 400,
      	width: 500
	});

	$('#dialog-waitD').dialog({
		autoOpen: false,
		modal: true,
		height: 400,
      	width: 500
	});


	$('#link_respaldo').click(function(){
		$.ajax({
			type: "POST",
			url: "http://localhost/Sistema/requestAjax.php",
			datatype: "json",
			data: {accion: '21'},
			beforeSend: function(){
				$('#dialog-wait').dialog("open");
			},
			success: function(response){
				console.log(response);
				$('#dialog-wait').dialog("close");
			},
			error: function(response){
				alert("Error. Consulte a su administrador del sistema.");
			}
		});
		return false;
	});

	$('#link_respaldo2').click(function(){
		$.ajax({
			type: "POST",
			url: "http://localhost/Sistema/requestAjax.php",
			data: {accion: '22'},
			beforeSend: function(){
				$('#dialog-waitD').dialog("open");
			},
			success: function(response){
				$('#dialog-waitD').dialog("close");
			},
			error: function(response){
				alert("Error. Consulte a su administrador del sistema.");
			}
		});
		return false;
	});

});