$(document).ready(function(){
	$('#rol').change(function(){
		if ($('#rol').val() == '3'){
			$('#divMaterias').addClass('hidden')
		}else{
			$('#divMaterias').removeClass('hidden')
		}
	});

	$("#nombre, #apellido").keydown(function (e) {
        // Allow: backspace, delete, tab, escape, enter and .
        if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
             // Allow: Ctrl+A
            (e.keyCode == 65 && e.ctrlKey === true) || 
             // Allow: home, end, left, right, down, up
            (e.keyCode >= 35 && e.keyCode <= 40)) {
                 // let it happen, don't do anything
                 return;
        }
        // Ensure that it is a number and stop the keypress
        if ( !((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105))) {
            e.preventDefault();
        }
    });

	/*$('#submit').click(function(){
		alert("Usuario guardado exitosamente.");
		return false;
	});*/

	$('#cedula').inputmask("99999999");
	$('.notNumbers').keypress(function(key){
		if( (key > 64 && key < 91) || (key > 192 && key < 254) ){
			alert(key);
		}else{
			return '';
		}

	});
	

	$('#cedula').focusout(function(){
		ci = $(this).val();
		$.ajax({
          type: "POST",
          url: "http://localhost/Sistema/requestAjax.php",
          data: {accion: '24', cedula: ''+ ci},
          success: function(response){
            if(response.indexOf('1') > 0){
            	$('#cedula').addClass('errorMsg');
            	$('#errorMens').html("Cedula usada");
				$('.btn-primary').addClass('disableClick');
            }else{
            	$('#cedula').removeClass('errorMsg');
            	$('#errorMens').html("");
				$('.btn-primary').removeClass('disableClick');
            	$(this).addClass('errorMsg');
            }

          },
          error: function(response){
            alert(response);
          }
        });
	})

	$("#submit_2").click(function(){
		alert("El nuevo usuario será agregado");
		$("#wizard").submit();
		
	});

	//------Bloquear Anchor y verif cedula--------//
	/*var preventClick = false;

	$('.btn-primary').click(function(e) {
	    $(this)
	       .css('cursor', 'default')
	       .css('text-decoration', 'none')

	    if (!preventClick) {
	        $(this).html($(this).html() + ' lalala');
	    }

	    preventClick = true;

	    return false;
	});*/


});