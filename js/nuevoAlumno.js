$(document).ready(function(){
	var fecha = new Date();
	var day = fecha.getDate();
	var month = fecha.getMonth() + 1;
	var year = fecha.getFullYear();

	$('#rep_leg').change(function(){
		if ($('#rep_leg').is(':checked')){
			$('#rep_nat').prop('checked', false);
			$('#rep_nat').removeAttr('required');
			$('#rep_leg').attr('required', 'required');
		}else{
			$('#rep_leg').attr('required', 'required');

		}	
	});
	
	$('#rep_nat').change(function(){
		if ($('#rep_nat').is(':checked')){
			$('#rep_leg').prop('checked', false);
			$('#rep_leg').removeAttr('required');
			$('#rep_nat').attr('required', 'required');
		}else{
			$('#rep_nat').attr('required', 'required');

		}
	});

	$('#fechaNacimiento, #rep_fechaNacimiento').change(function(){
		var fechaNacimiento = $('#fechaNacimiento').val();
		var res = fechaNacimiento.split("/");
		var edad, año, mes, dia;
		año	= res[2];
		mes = res[0];
		dia = res[1];
		
		edad = year - año;
		if(month < mes) {
			edad = edad - 1;
		}else {
			if(mes == month){
				if (dia < day) {
					edad = edad - 1;
				}
			}
		}

		if(edad <= 1){
			alert("Fecha de nacimiento no valida")
		}else{
			document.getElementById('edad').value = edad;
		}
	});

	$('#cedula, #rep_cedula').inputmask("99999999");
	$('#fechaIngreso').datepicker({
		monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ],
		dayNamesShort: [ "Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab" ],
		dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ]
	});
	$('#fechaNacimiento, #rep_fechaNacimiento').datepicker({
		changeMonth: true,
        changeYear: true,
		yearRange: '1900:2015',
		monthNames: [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"],
		monthNamesShort: [ "Ene", "Feb", "Mar", "Abr", "May", "Jun", "Jul", "Ago", "Sep", "Oct", "Nov", "Dic"],
		dayNamesShort: [ "Dom", "Lun", "Mar", "Mier", "Jue", "Vie", "Sab" ],
		dayNamesMin: [ "Do", "Lu", "Ma", "Mi", "Ju", "Vi", "Sa" ]
	});

	$('#emergencia, #rep_tel_principal, #rep_tel_secundario').inputmask("(999)9999999");
	$('#edad').inputmask("99");

	$("#wizard").submit(function(e){
		datInfo = $(this).serialize();
		console.log(datInfo);

		// ajax representante
		var representante = $('#rep_nombre').val() + '-';
		representante = representante + $('#rep_apellido').val() + '-';
		representante = representante + $('#rep_fechaNacimiento').val() + '-';
		representante = representante + $('#rep_cedula').val() + '-';
		representante = representante + $('#rep_genero').val() + '-';
		representante = representante + $('#rep_correo').val() + '-';
		representante = representante + $('#rep_tel_principal').val() + '-';
		representante = representante + $('#rep_tel_secundario').val() + '-';
		representante = representante + $('#rep_nacionalidad').val() + '-';
		representante = representante + $('#rep_direccion').val() + '-';
		

		var alumno = $('#nombre').val() + '-';
		alumno = alumno + $('#edad').val() + '-';
		alumno = alumno + $('#cedula').val() + '-';
		alumno = alumno + $('#grado').val() + '-';
		alumno = alumno + $('#nacionalidad').val() + '-';
		alumno = alumno + $('#emergencia').val() + '-';
		alumno = alumno + $('#apellidos').val() + '-';
		alumno = alumno + $('#fechaIngreso').val() + '-';
		alumno = alumno + $('#correo').val() + '-';
		alumno = alumno + $('#genero').val() + '-';
		alumno = alumno + $('#fechaNacimiento').val()+ '-';

		ciAlum = $('#cedula').val() + '-';
		var mater = '';
		$.each($('#materias').val(), function(key,value){
			mater = mater + value + '*' ;
		});
		mater = mater.slice(0,-1);
		alumno = alumno + mater;

		console.log(mater);
		$('#rep_leg').change(function(){
			if ($('#rep_leg').is(':checked')){
				representante = representante + 'legal';
			}else{
				representante = representante + 'natural';

			}
		});
		
		$.ajax({
          type: "POST",
          url: "http://localhost/Sistema/requestAjax.php",
          data: {accion: '20', rep : representante, alm: alumno},
          success: function(response){
            alert("Alumno agregado");
            window.open("http://localhost/Sistema/reporteInscr.php?id="+ciAlum);
            location.href = "./nuevoAlumno.php";   
          },
          error: function(response){
            alert("Error. Contacte con su administrador del sistema.");
          }
        });
		e.preventDefault(); //STOP default action
    	//e.unbind(); //unbind. to stop multiple form submit.
	});

	$("#btn_sub_alum").click(function(){
		alert("El alumno ha sido modificado");
		$("#frm_modif_alum").submit();
	});
});