$(document).ready(function(){
  $("#ciHab").inputmask("99999999");

  $("#cedulaHab").click(function(){
    if ($("#ciHab").val()) {
      var codigo = $("#ciHab").val();
      $.ajax({
        type: "POST",
        url: "http://localhost/Sistema/requestAjax.php",
        data: {accion: '17', cedula: ''+codigo},
        success: function(response){
          
          if(response == '0'){
            alert("la cedula introducida no corresponde con un alumno en sistema")
          }else{
            alert("El alumno ha sido habilitado.");
          }

        },
        error: function(response){
          alert("Error. Contacte con su administrador del sistema.");
        }
      }); 
    }else{
      alert("Indique el alumno que desea habilitar");
    }
    return false;
  });
});