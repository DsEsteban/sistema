<?php
	require("conexion_db.php");
	require_once('mpdf.php');
	$ci = $_POST['cedulaBol'];
	$consultaNotas = mysql_query("SELECT a.Materia, b.primerLapso, b.segundoLapso, b.tercerLapso FROM materias AS a 
									INNER JOIN calificacion AS b ON a.Codigo = b.codigo
									WHERE b.cedula = '$ci'");
	
	$consultaEstudiante = mysql_query("SELECT * FROM alumnos WHERE CI ='$ci'");
	$alumno = mysql_fetch_object($consultaEstudiante);

	$consultaRoot = mysql_query("SELECT * FROM usuarios WHERE nivel ='2'");
	$root = mysql_fetch_object($consultaRoot);

	$anho = "";
	switch ($alumno->Grado) {
		case '1':
			$anho = "1er año";
		break;
		case '2':
			$anho = "2do año";
		break;
		case '3':
			$anho = "3er año";
		break;
		case '4':
			$anho = "4to año";
		break;
		case '5':
			$anho = "5to año";
		break;
	}

	// periodo
		$idPeriodoConsulta = mysql_query("SELECT * FROM periodo order by id_periodo DESC LIMIT 1") or die('Mi error es: '.mysql_error());
		$periodo = mysql_fetch_object($idPeriodoConsulta);
		$periodoAnualIni = explode("-",$periodo->fecha_primero);
		
		$periodoArray = explode("/",$periodoAnualIni[0]);
		$periodoAnho = (int)$periodoArray[2];
	//	
	
	$mpdf = new mPDF('utf-8', 'A4');
	$mpdf->SetTitle("TITLE");
	$mpdf->SetAuthor("AUTHOR");
	$mpdf->SetWatermarkText("TEST");
	$mpdf->showWatermarkText = False;
	$mpdf->watermark_font = 'DejaVuSansCondensed';
	$mpdf->watermarkTextAlpha = 0.1;
	$mpdf->SetDisplayMode('fullpage');
	$margenUno = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$margen = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$margenDos = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
	$text = '';
	while($fila=mysql_fetch_row($consultaNotas)){
		$count = 3;
	   	$textdos = '<tr>
		   	<td>'.$fila['0'].'</td>
		   	<td>'.($fila['1'] === '0' ? '-' : $fila['1']).'<br><br><br></td>
		   	<td>'.($fila['2'] === '0' ? '-' : $fila['2']).'<br><br><br></td>
		   	<td>'.($fila['3'] === '0' ? '-' : $fila['3']).'<br><br><br></td>';
		   	
	    $fila['3'] === '0' ? $count-- : skip;   	   	 
	  	$fila['2'] === '0' ? $count-- : skip;
	  	
	  	if($fila['3'] === '0'){
			$text = $text.''.$textdos.'<td>-</td></tr>';

	  	}else{
			$text = $text.''.$textdos.'<td>'.round(((int)$fila['1'] + (int)$fila['2'] + (int)$fila['3'])/$count).'</td></tr>';
	  		
	  	}
  	}


	$tabla =	'<table cellspacing="0px" width="100%" border="1">
					<tr>
						<td border="0">Materia</td>
						<td border="0">Primer Lapso</td>
						<td border="0">Segundo Lapso</td>
						<td border="0">Tercer Lapso</td>
						<td border="0">Definitiva</td>
					</tr>'.
						$text
					.'</table>
					<br>
					<br>
					<br>
					<div width="100%" id="tabla-inferior">
						<table cellspacing="0px" width="70%" border="1">
							<tr>
								<th colspan ="12">Firmas</th>
							</tr>
							<tr>
								<td colspan ="4">Autoridad</td>
								<td colspan ="4">Alumno</td>
								<td colspan ="4">Representante</td>
							</tr>
							<tr>
								<td colspan ="4"><br><br><br><br><br><br></td>
								<td colspan ="4"><br><br><br><br><br><br></td>
								<td colspan ="4"><br><br><br><br><br><br></td>
							</tr>
						</table>
					</div>


					';

	$html = '<style type="text/css">
				#titulos{
					text-align:center;
				};
				#tabla-inferior{

				}

			</style>
			<div id="titulos">
				<table cellspacing="0px" width="100%" border="0">
					<tr>
						<td>
							<img src="./img/logo_colegio.png" width="90" height="85">
						</td>
						<td>
							<p>'.$margenUno.'República Bolivariana de Venezuela</p>
							<br>
							<p>&nbsp;&nbsp;&nbsp;&nbsp;Ministerio del poder popular para la Educación</p>
							<br>
							<p>'.$margenDos.'Liceo Augusto Pi Suñer</p>
						</td>
					</tr>
				</table>
				<h2>
					<span align="center" width="" height="">Boletín de Calificaciones</span>
				</h2>
				<br>
				<table>
					<tr>
						<td align="right"><p class="text-right"><strong>Alumno: </strong></p></td>
						<td align="left"><p class="text-right"><strong>'.$alumno->Nombre.' '.$alumno->Apellidos.$margen.'</strong></p></td>
						<td align="right"><p><strong>CI: </strong></p></td>
						<td align="left"><strong>'.$alumno->CI.'</strong></td>
					</tr>
					<tr>
						<td align="right"><p><strong>Año:  <strong></p></td>
						<td align="left"><p><strong>'.$anho.'</strong></p></td>
						<td align="right"><p><strong>Periodo:  </strong></p></td>
						<td align="left"><p><strong>20'.$periodoAnho.'/20'.($periodoAnho + 1).'</strong></p></td>
					</tr>
					<tr>
						<td align="right"><strong>Lapso:</strong>  </td>
						<td align="left"><strong>'.$root->lapso.'</strong></td>
						<td align="right"><strong>Estatus:</strong>  </td>
						<td align="left"><strong>'.($alumno->estatus === '0' ? 'deshabilitado' : 'habilitado').'</strong></td>
					</tr>
				</table>
				<br>
			</div>
	'.$tabla;
	$mpdf->WriteHTML($html);
	$mpdf->Output();
	mysql_close($con);
	exit;
	// Fin de mPDF.
?>