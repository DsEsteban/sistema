<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>Sistema</title>
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<meta name="description" content="Avant" />
	<meta name="author" content="The Red Team" />

    <link href="./Avant/assets/css/styles.min.css" rel="stylesheet" type='text/css' media="all" />
    <link href='./Avant/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='styleswitcher' /> 
    <link href='./Avant/assets/demo/variations/default.css' rel='stylesheet' type='text/css' media='all' id='headerswitcher' /> 
	<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/codeprettifier/prettify.css' /> 
	<link rel='stylesheet' type='text/css' href='./Avant/assets/plugins/form-toggle/toggles.css' /> 
	<link rel='stylesheet' type='text/css' href='./Avant/assets/MultiSelect/css/multi-select.css' /> 
	<link rel='stylesheet' type='text/css' href='./css/ingresoRoot.css' /> 

	<style>
	</style>

	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head>

<body class="">
    <div id="headerbar">
        <div class="container"></div>
    </div>

	<header class="navbar navbar-inverse navbar-fixed-top" role="banner">
        <div class="navbar-header pull-left col-md-7">
            <div class="col-md-offset-1 col-md-2">
            
                
            <img id="imagenLogo" src="./img/logo_colegio.png" alt="logo colegio augusto pi suñer" width="90" height="95" data-trigger="hover" data-container="body" data-toggle="popover" data-placement="bottom" data-content="El liceo bolivariano augusto pi Suñer está Centrada en un servicio educativo y eficaz el cual contribuya con la formación de los alumnos el cual construya una disciplina con el objetivo de alcanzar un nivel educativo virtuoso, que sustente valores, ética y un modelo de educación con calidad.">
            
            </div>
            <div class="col-md-12">
                <h1><span>Liceo Augusto Pi Su&ntilde;er</span></h1>
            </div>
        </div>
    </header>

    <div id="page-container">
        <!-- BEGIN SIDEBAR -->
      

			<!-- BEGIN RIGHTBAR -->
			<!-- END RIGHTBAR -->
<div id="page-content">
    <div id='wrap'>
		<div id="page-heading">
        	<h1><strong>Nuevo Usuario</strong></h1>
      	</div>
    <div class="container">


<div class="row">
    <div class="col-md-12">

		<div class="panel panel-info">
			<div class="panel-heading">
				<h4><span>Recuperaci&oacute;n de contrase&ntilde;a</span></h4>
			</div>
			<div class="panel-body">
				<form class="form-horizontal row-border">
					<div id = "respuesta" class="form-group">
						<label for="fieldname" class="col-md-3 control-label">Cedula:</label>
						<div class="col-md-6">
							<input id="cedula" class="form-control" name="cedula" placeholder ="Cedula" minlength="4" maxlength="20" type="text" required="" />
						</div>
						<button id="btnCed">Enviar</button>
						
					</div>

					<div class="hidden" id="response">
						<br>
						<p id="pregunta"></p>
					</div>
					
					<div id = "respuestaDos" class="form-group hidden">
						<label for="fieldname" class="col-md-3 control-label">Respuesta secreta:</label>
						<div class="col-md-6">
							<input id="respuestaSec" class="form-control" name="respuestaSec" placeholder ="Respuesta Secreta" minlength="4" maxlength="20" type="text" required="" />
						</div>
						<button id="btnVer">Verificar</button>
					</div>

					<div id = "newPassDiv" class="form-group hidden">
						<label for="fieldname" class="col-md-3 control-label">Contraseña nueva</label>
						<div class="col-md-6">
							<input id="claveNew" class="form-control" name="claveNew" placeholder ="Contraseña nueva" minlength="4" maxlength="20" type="password" required="" />
						</div>
						<button id="btnNew">Cambiar</button>
					</div>

				</form>
			</div>

			<div class="panel-footer">
				<a class="btn-default btn" onclick=" location.href='./index.php' " />Volver</a>
			</div>
		</div>
    </div>
</div>


</div> <!-- container -->
</div> <!--wrap -->
</div> <!-- page-content -->
</div> <!-- page-container -->
<script type='text/javascript' src='./Avant/assets/js/jquery-1.10.2.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jqueryui-1.10.3.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/bootstrap.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/enquire.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.cookie.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.touchSwipe.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/jquery.nicescroll.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/codeprettifier/prettify.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/easypiechart/jquery.easypiechart.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/sparklines/jquery.sparklines.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-toggle/toggle.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-validation/jquery.validate.min.js'></script> 
<script type='text/javascript' src='./Avant/assets/plugins/form-stepy/jquery.stepy.js'></script> 
<script type='text/javascript' src='./Avant/assets/demo/demo-formwizard.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/placeholdr.js'></script> 
<script type='text/javascript' src='./Avant/assets/js/application.js'></script> 
<script type='text/javascript' src='./Avant/assets/demo/demo.js'></script> 
<script type='text/javascript' src='./Avant/assets/MultiSelect/js/jquery.multi-select.js'></script> 
<script type='text/javascript' src='./js/maskJs/jquery.inputmask.js'></script>
<script type='text/javascript' src='./js/jquery.form-validator.js'></script>
<script type='text/javascript' src='./js/mi_inscripcion.js'></script>
<!--<script type='text/javascript' src='./js/mi_pass.js'></script>-->
</body>
</html>
